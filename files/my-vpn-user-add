#!/bin/bash
set -Eeuo pipefail

CLIENT="$1"
CLIENTCONF="/root/$1.ovpn"
export EASYRSA="/etc/openvpn/ca"                     ### easyrsa CA path
export EASYRSA_PKI="${EASYRSA}/pki"

if [ -z "$CLIENT" ]; then
  echo "Usage: $0 <clientname>"
  exit 1
fi
if [ $UID -ne 0 ]; then
  echo "ERROR: I need to be root"
  exit 2
fi

if [ ! -e "${EASYRSA_PKI}/private/${CLIENT}.key" ]; then
    ${EASYRSA}/easyrsa build-client-full ${CLIENT} nopass
fi


cat <<-EOF >${CLIENTCONF}
	## NOTE:
	## ubuntu uses systemd-resolved, so the config works out of the box, if your system uses resolvconf you'll have to change the up/down scripts to have the DNS updated properly
	## following *client* packages are needed on a default ubuntu desktop install - adjust accordingly
	## apt-get install openvpn network-manager-openvpn network-manager-openvpn-gnome openvpn-systemd-resolved
	client
	dev tun
	proto udp
	comp-lzo
	remote ${HOSTNAME} 1194
	resolv-retry infinite
	nobind
	remote-cert-tls server
	auth SHA512
	cipher AES-256-CBC
	compress lz4-v2
	explicit-exit-notify 1
	verb 3
	#script-security 2
	#up /etc/openvpn/update-systemd-resolved
	#down /etc/openvpn/update-systemd-resolved
	#up /etc/openvpn/update-resolv-conf
	#down /etc/openvpn/update-resolv-conf

EOF

echo "<ca>" >>${CLIENTCONF}
cat ${EASYRSA_PKI}/ca.crt >>${CLIENTCONF}
echo "</ca>" >>${CLIENTCONF}


echo "<cert>" >>${CLIENTCONF}
cat ${EASYRSA_PKI}/issued/${CLIENT}.crt >>${CLIENTCONF}
echo "</cert>" >>${CLIENTCONF}


echo "<key>" >>${CLIENTCONF}
cat ${EASYRSA_PKI}/private/${CLIENT}.key >>${CLIENTCONF}
echo "</key>" >>${CLIENTCONF}

chmod 600 ${CLIENTCONF}

echo '################################################################################################################'
echo "below the client config file, copy and paste exactly everything eblow. in case of a doubt the file is also stored here: ${CLIENTCONF}"
echo '################################################################################################################'

cat ${CLIENTCONF}
